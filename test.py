import time

from InstagramAPI import InstagramAPI
from instabot import Bot

from constants import LOGIN, PASSWORD


def all_followers(login, password):
    api = InstagramAPI(login, password)
    api.login()

    api.getSelfUsernameInfo()
    username_id = api.LastJson['user']['pk']

    list_users_ids = []
    big_list = True

    api.getUserFollowers(usernameId=username_id)
    result = api.LastJson
    # print(result)

    next_max_id = None
    try:
        next_max_id = result['next_max_id']
    except:
        print("Haven't next_max_id's")

    # forming list_users_ids
    for i in range(len(result['users'])):
        list_users_ids.append(result['users'][i]['pk'])

    while big_list == True:
        api.getUserFollowers(usernameId=username_id, maxid=next_max_id)
        result = api.LastJson

        for i in range(len(result['users'])):
            list_users_ids.append(result['users'][i]['pk'])

        try:
            next_max_id = result['next_max_id']
            big_list = result['big_list']
        except:
            big_list = False

    for i in list_users_ids:
        print(i)

    return list_users_ids

list = all_followers('vkysnjashka_shokoladka', 'Ru_ru_89')
# all_followers('380508312031', '46256As61F2qwerty')

api = InstagramAPI('vkysnjashka_shokoladka', 'Ru_ru_89')
api.login()

msg = '💕 Шоколадная эйфория 💕 \n 🛍 Сладости ручной работы из Бельгийского шоколада \n 📣Натуральные ингредиенты  \n 📬Отправка по всей Украине 🇺🇦 и СНГ 🌍 \n💰Скидка на первый заказ. Заходи выбирай покупай:)'

for i in range(150, len(list)):
    api.direct_message(msg, list[i])
    print(i + 1, "id:", list[i])
    time.sleep(3)
    if i == 155:
        time.sleep(60*2)
    elif i%100 == 0:
        time.sleep(60*5)
