import telebot
import time

from constants import token

from InstagramAPI import InstagramAPI

bot = telebot.TeleBot('720910962:AAHE_5UkzVCMBtxgewC1-c--a8f_CKLTHaM')
login = None
password = None
message_to_followers = None


def all_followers(login, password):
    api = InstagramAPI(login, password)
    api.login()

    api.getSelfUsernameInfo()
    username_id = api.LastJson['user']['pk']

    list_users_ids = []
    big_list = True

    api.getUserFollowers(usernameId=username_id)
    result = api.LastJson
    # print(result)

    next_max_id = None
    try:
        next_max_id = result['next_max_id']
    except:
        print("Haven't next_max_id's")

    # forming list_users_ids
    for i in range(len(result['users'])):
        list_users_ids.append(result['users'][i]['pk'])

    while big_list == True:
        api.getUserFollowers(usernameId=username_id, maxid=next_max_id)
        result = api.LastJson

        for i in range(len(result['users'])):
            list_users_ids.append(result['users'][i]['pk'])

        try:
            next_max_id = result['next_max_id']
            big_list = result['big_list']
        except:
            big_list = False

    return list_users_ids


@bot.message_handler(commands=['start'])
def send_welcome(message):
    user_murkup = telebot.types.ReplyKeyboardMarkup(True)
    user_murkup.row('/start')
    user_murkup.row('Да', 'Нет')
    msg = bot.send_message(message.from_user.id, "Привет☺☺☺ ты хочешь отправить сообщение всем своим подписчикам???",
                           reply_markup=user_murkup)
    bot.register_next_step_handler(msg, login_step)


def login_step(message):
    if message.text == 'Да':
        msg  = bot.send_message(message.from_user.id, "Введи логин к твоему Instagram")
        bot.register_next_step_handler(msg, password_step)
    elif message.text == 'Нет':
        msg = bot.send_message(message.from_user.id, "Всего самого лучшего")
        bot.register_next_step_handler(msg, send_welcome)
    else:
        msg = bot.send_message(message.from_user.id, "Бот не понимает вас, чтобы начать заново нажмите /start")
        bot.register_next_step_handler(msg, send_welcome)


def password_step(message):
    global login
    if message.text:
        login = message.text
        print("login", login)
        msg = bot.send_message(message.from_user.id, "Введите пароль к вашему Instagram")
        bot.register_next_step_handler(msg, messageToFollowers)
    else:
        msg = bot.send_message(message.from_user.id, "Ошибка !!! Нажмите /start")
        bot.register_next_step_handler(msg, send_welcome)


def messageToFollowers(message):
    global password
    if message.text:
        password = message.text
        print("password:", password)
        api = InstagramAPI(login, password)
        if api.login():
            msg = bot.send_message(message.from_user.id, "Введите ваше сообщение")
            bot.register_next_step_handler(msg, sending_step)
        else:
            msg = bot.send_message(message.from_user.id, "Неверный логин или пароль, чтобы попробовать снова нажмите /start")
            bot.register_next_step_handler(msg, send_welcome)
    else:
        msg = bot.send_message(message.from_user.id, "Ошибка !!! Нажмите /start")
        bot.register_next_step_handler(msg, send_welcome)


def sending_step(message):
    global message_to_followers
    if message.text:
        message_to_followers = message.text
        print("message:", message_to_followers)


        api = InstagramAPI(login, password)
        if api.login():
            list_ids = all_followers(login, password)
            bot.send_message(message.from_user.id, "Получено список твоих подписчиков Instagram.")

            # sending messages
            for i in range(len(list_ids)):
                api.direct_message(message_to_followers, list_ids[i])
                print(i+1, list_ids[i])
                time.sleep(3)

                if i%100 == 0 and i != 0:
                    time.sleep(17)
                    m = "Cообщение отправлено к {0} подписчикам".format(i)
                    bot.send_message(message.from_user.id, m)

                if i%200 == 0:
                    api.logout()
                    time.sleep(5)
                    api.login()

            bot.send_message(message.from_user.id, "Сообщение отправлено всем подписчикам")

        else:
            msg = bot.send_message(message.from_user.id, "Ошибка !!! Нажмите /start")
            bot.register_next_step_handler(msg, send_welcome)


while True:

    try:

        bot.polling(none_stop=True)

    # ConnectionError and ReadTimeout because of possible timout of the requests library

    # TypeError for moviepy errors

    # maybe there are others, therefore Exception

    except Exception as e:


        time.sleep(2)

